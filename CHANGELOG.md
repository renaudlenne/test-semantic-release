# [1.1.0](https://gitlab.com/renaudlenne/test-semantic-release/compare/v1.0.1...v1.1.0) (2020-12-03)


### Features

* **pataplouf:** a whole new file this time ([4c1d542](https://gitlab.com/renaudlenne/test-semantic-release/commit/4c1d542ff25726c9e2d3761c4f9eda9f4a511e1e)), closes [#111](https://gitlab.com/renaudlenne/test-semantic-release/issues/111)
* **pouet:** a whole complete new line! ([49c6360](https://gitlab.com/renaudlenne/test-semantic-release/commit/49c6360a88b3590006751293fdbb5eb19e87e0ef)), closes [#26](https://gitlab.com/renaudlenne/test-semantic-release/issues/26)

## [1.0.1](https://gitlab.com/renaudlenne/test-semantic-release/compare/v1.0.0...v1.0.1) (2020-12-03)


### Bug Fixes

* **pouet:** added a question ([6c319d4](https://gitlab.com/renaudlenne/test-semantic-release/commit/6c319d4c29d9f05e0749b540d92f3919380f4dd8)), closes [#65](https://gitlab.com/renaudlenne/test-semantic-release/issues/65)

# 1.0.0 (2020-12-03)


### Features

* **pouet:** a completly new text file ([0c63580](https://gitlab.com/renaudlenne/test-semantic-release/commit/0c63580976b716cd982a47e20e9681b58cd6c73b)), closes [#42](https://gitlab.com/renaudlenne/test-semantic-release/issues/42)
